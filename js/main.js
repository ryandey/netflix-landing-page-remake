const tabItems = document.querySelectorAll('.tab-item');
const tabContentItems = document.querySelectorAll('.tab-content-item');

// Select tab content item
function selectItem(e) {
    removeBorder();
    removeShow();

    // Add border to current tab
    this.classList.add('tab-border');

    // Grab content item from DOM
    const tabContentItem = document.querySelector(`#${this.id}-content`);

    // Add show class to content
    tabContentItem.classList.add('show');
}

// Remove border from previously selected tab(s)
function removeBorder() {
    tabItems.forEach(item => item.classList.remove('tab-border'));
}

// Remove 'show' class from previously selected tab(s)
function removeShow() {
    tabContentItems.forEach(item => item.classList.remove('show'));
}

// Listen for tab click
tabItems.forEach(item => item.addEventListener('click', selectItem));